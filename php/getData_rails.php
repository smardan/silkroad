   
<?php include "include/db.php";

/*
 * Author:  Somi Dasom Choi
 * Author email: dasom.choi@sait.ca
 * Created:  Feb.2019
 * 
 * (c) Copyright by Silk Road Team - CIRUS, ARIS, SAIT.
*/

#query
$strQry="SELECT ST_AsGEOJSON(geom, 7) as geom FROM modern_routes.rails_routes";


if (isset($_POST['filter'])) {
        if ($_POST['filter']!=='All') {
            $strQry="SELECT ST_AsGEOJSON(geom, 7) as geom FROM modern_routes.rails_routes WHERE category='{$_POST['filter']}'";
        }
    }

    #get message if the query and connection are not working
        $sql = $conn->query($strQry);
        if (!$sql) {
            echo 'An error occured';
            exit;
        }
//create empty array
    $features=[];

    #loop through rows to build feature arrays
    while($row = $sql->fetch(PDO::FETCH_ASSOC)) {
        $feature=['type'=>'Feature'];
        $feature['geometry']=json_decode($row['geom']);
        

        #remove geometry fields from properties
        unset($row['geom']);
        
        $feature['properties']=$row;
        
        #add feature arrays to feature collection array
        array_push($features, $feature);
    }
    
    $featureCollection=['type'=>'FeatureCollection', 'features'=>$features];


//takes an associative array and turns it into a JSON string
    echo json_encode($featureCollection);
     
    return $conn;
   
    pg_close($conn); 
?>


