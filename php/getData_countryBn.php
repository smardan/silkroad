<?php include "include/db.php";

    #query
    $strQry="SELECT ST_AsGEOJSON(geom) as geom FROM country_bn";

    if (isset($_POST['filter'])) {
            if ($_POST['filter']!=='All') {
                $strQry="SELECT ST_AsGEOJSON(geom) as geom FROM country_bn WHERE category='{$_POST['filter']}'";
            }
        }

    #db connection
        $conn = new PDO('pgsql:host=sait.bgis.gismapping.ca;dbname=silkroad;', 'postgres', 'password');

    #refer php file to connect to the db
    #require 'php/connectDb';

        $sql = $conn->query($strQry);
        if (!$sql) {
            echo 'An SQL error occured.';
            exit;
        }
        
//create empty array
    $features=[];

    #loop through rows to build feature arrays
    while($row = $sql->fetch(PDO::FETCH_ASSOC)) {
        $feature=['type'=>'Feature'];
        $feature['geometry']=json_decode($row['geom']);
        
     
        #remove geometry fields from properties
        unset($row['geom']);
        
        $feature['properties']=$row;
        
        #add feature arrays to feature collection array
        array_push($features, $feature);
    }
    
    $featureCollection=['type'=>'FeatureCollection', 'features'=>$features];


//takes an associative array and turns it into a JSON string
    echo json_encode($featureCollection);
   

    return $conn;

    pg_close($conn);
?>

