<!--
/*****
 * Author:  Somi Dasom Choi
 * Author email: dasom.choi@sait.ca
 * Created:  Feb.2019
 * 
 * (c) Copyright by Silk Road Team - CIRUS, ARIS, SAIT.
 *****/
-->

<?php ob_start(); ?>


<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Silk Road</title>

	<meta name="description" content="">
	<meta name="author" content="Somi Choi-dasom.choi@sait.ca">

	<link rel="icon" href="lib/favicon.ico">

	<!--**load Bootstrap core css//-->
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<link href="lib/bootstrap/css/build.min.css" rel="stylesheet" />

	<!--**load Bootstrap tour//-->
	<link href="lib/bootstrap/css/bootstrap-tour.min.css" rel="stylesheet">

	<!--**load Leaflet//-->
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" integrity="sha512-07I2e+7D8p6he1SIM+1twR5TIrhUQn9+I6yjqD53JQjFiMf8EtC93ty0/5vJTZGF8aAocvHYNEDJajGdNx1IsQ==" crossorigin="" />
	<script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
	
	<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet-src.js" integrity="sha512-WXoSHqw/t26DszhdMhOXOkI7qCiv5QWXhH9R7CgvgZMHz1ImlkVQ3uNsiQKu5wwbbxtPzFXd1hK4tzno2VqhpA==" crossorigin=""></script>

	<!--**Stamen Toner map tile//-->
	<script type="text/javascript" src="http://maps.stamen.com/js/tile.stamen.js?v1.3.0"></script>

	<!--**leaflet marker clusting plugin//-->
	<link rel="stylesheet" href="lib/bootstrap/css/style.css" />
	<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
	<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
	<script src="lib/bootstrap/js/script.js"></script>
	<script src="https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js"></script>

	<!--home button plugin-->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="lib/leaflet/leaflet.zoomhome.css" />
	<script src="lib/leaflet/leaflet.zoomhome.min.js"></script>

	<!--meaure tool plugin-->
	<link rel="stylesheet" href="lib/leaflet/leaflet.measure.css">
	<script src="lib/leaflet/leaflet.measure.js"></script>
      
	<!--**IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<link href="lib/bootstrap/css/ie10-viewport-bug-workaround.css" rel="stylesheet" />

	<!--**Custom styles for this template -->
	<link href="lib/dashboard/dashboard.css" rel="stylesheet" />
	<script src="lib/bootstrap/js/ie-emulation-modes-warning.js"></script>

	<!--**load Esri Leaflet from CDN -->
	<script src="https://unpkg.com/esri-leaflet/dist/esri-leaflet.js"></script>

	<!--**load Esri Leaflet Geocoder from CDN -->
	<link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.2.4/dist/esri-leaflet-geocoder.css">
	<script src="https://unpkg.com/esri-leaflet-geocoder@2.2.4"></script>

	<!--**To use an AJAX request, load JQuery library//-->
	<script src="lib/leaflet/leaflet.ajax.js"></script>
	<script src="lib/turf.min.js"></script>

	<!--**Makes reference to the AJAX jQuery lib-->
	<script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
	<script src="https://code.jquery.com/jquery-2.1.1.js"></script>
	
	<!--**load Bootstrap tour//-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap-tour.min.js"></script>

	<!--**jQuery UI themes & pips library-->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script src="lib/jquery/jquery-ui-slider-pips.js"></script>
	<script src="lib/jquery/jquery-ui-slider-pips.min.js"></script>
	<link rel="stylesheet" href="lib/jquery/jquery-ui-slider-pips.css">
	<link rel="stylesheet" href="lib/jquery/jquery-ui-slider-pips.min.css">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/hot-sneaks/jquery-ui.css">
	

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<!--**custom css-->
	<link rel="stylesheet" type="text/css" href="lib\bootstrap\css\silkroadStyle.css">

</head>
