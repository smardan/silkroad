<html>
<body>
<!--
/*
 * Author:  Somi Dasom Choi
 * Author email: dasom.choi@sait.ca
 * Created:  Feb.2019
 * 
 * (c) Copyright by Silk Road Team - CIRUS, ARIS, SAIT.
 */        
-->

 <!-- time slider  -->      
 <div id="pips-slider" class="pips-slider"></div>

	<!-- footer  -->
	<div class="footer">
	
		<div style="height:6px;">
			<!--flags-->
		<input type ="button" id="afg" title = "Afghanistan" class = "imageCS">
        <input type ="button" id="alb" title = "Albania" class = "imageCS">
        <input type ="button" id="are" title = "United Arab Emirates" class = "imageCS">
        <input type ="button" id="arm" title = "Armenia" class = "imageCS">
        <input type ="button" id="aut" title = "Austria" class = "imageCS">
        <input type ="button" id="aze" title = "Azerbaijan" class = "imageCS">
        <input type ="button" id="bel" title = "Belgium" class = "imageCS">
        <input type ="button" id="bfa" title = "Burkina Faso" class = "imageCS">
        <input type ="button" id="bgd" title = "Bangladesh" class = "imageCS">
        <input type ="button" id="bgr" title = "Bulgaria" class = "imageCS">
        <input type ="button" id="bhr" title = "Bahrain" class = "imageCS">
        <input type ="button" id="bih" title = "Bosnia" class = "imageCS">
        <input type ="button" id="blr" title = "Belarus" class = "imageCS">
        <input type ="button" id="btn" title = "Bhutan" class = "imageCS">
        <input type ="button" id="caf" title = "Central African Republic" class = "imageCS">
        <input type ="button" id="che" title = "Switzerland" class = "imageCS">
        <input type ="button" id="chn"title = "China" class = "imageCS">
        <input type ="button" id="civ" title = "Ivory Coast" class = "imageCS">
        <input type ="button" id="cyp" title = "Cyprus" class = "imageCS">
        <input type ="button" id="cze" title = "Czechia" class = "imageCS">
        <input type ="button" id="deu" title = "Germany" class = "imageCS">
        <input type ="button" id="dnk" title = "Denmark" class = "imageCS">
        <input type ="button" id="dza" title = "Algeria" class = "imageCS">
        <input type ="button" id="egy" title = "Egypt" class = "imageCS">
        <input type ="button" id="eri" title = "Eritrea" class = "imageCS">
        <input type ="button" id="esp" title = "Spain" class = "imageCS">
        <input type ="button" id="est" title = "Estonia" class = "imageCS">
        <input type ="button" id="eth" title = "Ethiopia" class = "imageCS">
        <input type ="button" id="fra" title = "France" class = "imageCS">
        <input type ="button" id="gbr" title = "United Kingdom" class = "imageCS">
        <input type ="button" id="geo" title = "Georgia" class = "imageCS">
        <input type ="button" id="gha" title = "Ghana" class = "imageCS">
        <input type ="button" id="gin" title = "Guinea" class = "imageCS">
        <input type ="button" id="grc"title = "Greece" class = "imageCS">
        <input type ="button" id="hrv" title = "Croatia" class = "imageCS">
        <input type ="button" id="hun" title = "Hungary" class = "imageCS">
        <input type ="button" id="ind" title = "India" class = "imageCS">
        <input type ="button" id="irn" title = "Iran" class = "imageCS">
        <input type ="button" id="irq" title = "Iraq" class = "imageCS">
        <input type ="button" id="isr" title = "Israel" class = "imageCS">
        <input type ="button" id="ita" title = "Italy" class = "imageCS">
        <input type ="button" id="jor" title = "Jordan" class = "imageCS">
        <input type ="button" id="jpn" title = "Japan" class = "imageCS">
        <input type ="button" id="kaz"title = "Kazakhstan" class = "imageCS">
        <input type ="button" id="kgz" title = "Kyrgyzstan" class = "imageCS">
        <input type ="button" id="khm" title = "Cambodia" class = "imageCS">
        <input type ="button" id="kor" title = "South Korea" class = "imageCS">
        <input type ="button" id="kwt" title = "Kuwait" class = "imageCS">
        <input type ="button" id="lao" title = "Laos" class = "imageCS">
        <input type ="button" id="lbn" title = "Lebanon" class = "imageCS">
        <input type ="button" id="lby" title = "Libya" class = "imageCS">
        <input type ="button" id="ltu" title = "Lithuania" class = "imageCS">
        <input type ="button" id="lva" title = "Latvia" class = "imageCS">
        <input type ="button" id="mar" title = "Morocco" class = "imageCS">
        <input type ="button" id="mda" title = "Moldova" class = "imageCS">
        <input type ="button" id="mkd" title = "Macedonia" class = "imageCS">
        <input type ="button" id="mli" title = "Mali" class = "imageCS">
        <input type ="button" id="mmr" title = "Myanmar" class = "imageCS">
        <input type ="button" id="mng" title = "Mongolia" class = "imageCS">
        <input type ="button" id="mrt" title = "Mauritania" class = "imageCS">	
        <input type ="button" id="mys" title = "Malaysia" class = "imageCS">
        <input type ="button" id="ner" title = "Niger" class = "imageCS">
        <input type ="button" id="nga" title = "Nigeria" class = "imageCS">			
        <input type ="button" id="nld" title = "Netherlands" class = "imageCS">
        <input type ="button" id="nor" title = "Norway" class = "imageCS">
        <input type ="button" id="npl" title = "Nepal" class = "imageCS">
        <input type ="button" id="omn" title = "Oman" class = "imageCS">
        <input type ="button" id="pak" title = "Pakistan" class = "imageCS">
        <input type ="button" id="pol" title = "Poland" class = "imageCS">
        <input type ="button" id="prk" title = "North Korea" class = "imageCS">
        <input type ="button" id="prt" title = "Portugal" class = "imageCS">
        <input type ="button" id="pse" title = "Palestine" class = "imageCS">
        <input type ="button" id="qat" title = "Qatar" class = "imageCS">
        <input type ="button" id="rou" title = "Romania" class = "imageCS">
        <input type ="button" id="rus" title = "Russia" class = "imageCS">
        <input type ="button" id="sau" title = "Saudi Arabia" class = "imageCS">
        <input type ="button" id="sdn" title = "Sudan" class = "imageCS">
        <input type ="button" id="sen" title = "Senegal" class = "imageCS">			
        <input type ="button" id="sgp" title = "Singapore" class = "imageCS">
        <input type ="button" id="som" title = "Somalia" class = "imageCS">
        <input type ="button" id="srb" title = "Serbia" class = "imageCS">
        <input type ="button" id="svk" title = "Slovakia" class = "imageCS">
        <input type ="button" id="svn" title = "Slovenia" class = "imageCS">
        <input type ="button" id="swe" title = "Sweden" class = "imageCS">
        <input type ="button" id="syr" title = "Syria" class = "imageCS">
        <input type ="button" id="tcd" title = "Chad" class = "imageCS">
        <input type ="button" id="tha" title = "Thailand" class = "imageCS">
        <input type ="button" id="tjk" title = "Tajikistan" class = "imageCS">
        <input type ="button" id="tkm" title = "Turkmenistan" class = "imageCS">
        <input type ="button" id="tun" title = "Tunisia" class = "imageCS">
        <input type ="button" id="tur" title = "Turkey" class = "imageCS">
        <input type ="button" id="twn" title = "Taiwan" class = "imageCS">
        <input type ="button" id="tza" title = "Tanzania" class = "imageCS">
        <input type ="button" id="uzb" title = "Uzbekistan" class = "imageCS">
        <input type ="button" id="vnm" title = "Vietnam" class = "imageCS">
        <input type ="button" id="yem" title = "Yemen" class = "imageCS">
        
		<span><img src="../silkroad/img/flags/nuskan.png" style="opacity:1;width:16px;height:10px;float:right;margin-right:6px;margin-top:5px;"></span>
        <!-- <span><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9Ctnr4rqDYLDEBuAGqj-aeZpvehggPjfsN0iBOTCyB6lOqcf4Ng" style="width:13px;height:10px;float: right;margin-right:4px;margin-top:5px;"></span> -->

        <span style="float:right;margin-right:6px;margin-top: 3px;">
            <p> Supported by</p>
        </span>
	   </div>
	   <br>
	   <h6 id="map_coords" class="text-center" style="font-size: 12px; font-family: inherit;">Latitude: 29.5, Longitude: 63, Zoom: 3.5</h6>
	   <h6 class="text-center" style="font-size: 12px; font-family: inherit;">&copy;<?php echo date("Y");?> <a href="http://www.saitarisblog.com/category/cirus/" target='blank'style="font-weight:bold; ";>CIRUS, SAIT-Silk Road</a></h6>
	</div>

	<div id="log"></div>

	<script src="lib/main.js"></script>


	<!--================================================================-->
	<!-- Place at the end of the document so the pages load faster -->

	<script>
		window.jQuery || document.write('<script src="lib/bootstrap/js/jquery.min.js"><\/script>')

	</script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="lib/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

</body>

</html>