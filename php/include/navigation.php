<!--
/*****
 * Author:  Somi Dasom Choi
 * Author email: dasom.choi@sait.ca
 * Created:  Feb.2019
 * 
 * (c) Copyright by Silk Road Team - CIRUS, ARIS, SAIT.
 *****/
-->
   
   
<div class="navigation-bar">
    <div class="navbar-header">
        <a class="navbar-brand" style="color: white" ;><img src="https://www.sait.ca/assets/images/logo.png" id="logo" width="30" height="40"> Silk Road</a>
    </div>
    <ul class="nav navbar-nav">
        <li class="active"><a href="#" style="color: white; margin-top: -15px;"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" style="font-size: 16px;">Home</button></a></li>
        <li><a href="#" style="color: white; margin-top: -15px;"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="font-size: 16px;">Upload Data</button></a></li>
        <li><a href="#" style="color: white; margin-top: -15px;"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" style="font-size: 16px;">Data Inquiry</button></a></li>
        <li class="active"><a href="#" style="color: white; margin-top: -15px;"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" style="font-size: 16px;">Geoprocessing Tool</button></a></li>
    </ul>


    <ul class="nav navbar-nav navbar-right">

        <li><a href="#" style="color: white; margin-top: -15px;"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#signUp" style="font-size: 16px;"><span class="glyphicon glyphicon-user" style="color: white" ;></span> Sign Up</button></a></li>
        <!--			<li><a href="#" style="color: white"><span class="glyphicon glyphicon-user" data-toggle="modal"  data-target="#signup";></span> Sign Up</a></li>-->

        <li><a href="#" style="color: white; margin-top: -15px;"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#logIn" style="font-size: 16px;"><span class="glyphicon glyphicon-log-in" style="color: white" ;></span> Login</button></a></li>

        <li><a href="https://drive.google.com/file/d/1w6Wbb9X4gDalP7qnboU-y3r6ldz2EtM6/view?usp=sharing" data-toggle="modal" target='blank' style="color: white; margin-right: 20px" ;>Help</a></li>
        <!---embed pdf to the help document for users instruction--->
    </ul>
</div>

<div class="searchbox">
    <input class="search" type="text" name="search" placeholder="Search By Country" onkeyup="searchq();" />
</div>

<table class="result table-bordered" id="output"></table>


<!---Model plugin for upload data -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please fill in and submit the form below (* fields are required.) </h4>
            </div>
            <div class="modal-body" style="">
                <form action="" autocomplete="on">
                    * Country:<br> <select name="country">
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="China">China</option>
                        <option value="Egypt">Egypt</option>
                        <option value="Greece">Greece</option>
                        <option value="India">India</option>
                        <option value="Iran">Iran</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhastan">Kazakhastan</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Laos">Laos</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Nepal">Nepal</option>
                        <option value="North Korea">North Korea</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Singapore">Singapore</option>
                        <option value="South Korea">South Korea</option>
                        <option value="Syria">Syria</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                    </select>
                    <br><br />

                    * Category:<br> <select name="category">
                        <option value="Airport" style="color:rgb(211, 196, 79)">Airport</option>
                        <option value="Railway" style="color:rgb(211, 196, 79)">Railway</option>
                        <option value="Caravanserai" style="color:rgb(115, 184, 224);">Caravanserai</option>
                        <option value="Religion Monuments" style="color:rgb(115, 184, 224);">Religion Monuments</option>
                        <option value="Land_cover" style="color: yellowgreen">Land Cover</option>
                        <option value="Wild_life" style="color: yellowgreen">Wild Life</option>
                        <option value="Land_cover" style="color: yellowgreen">Land Cover</option>
                    </select>
                    <br><br />

                    * Name:<br> <input type="name" name="name" autocomplete="off"><br><br />

                    Image (.jpg, .png, .gif, .tiff):
                    <form action="upload.php" method="POST" enctype="multipart/form-data">
                        <input type="file" name="fileToUpload" id="fileToUpload">
                    </form>
                    <br>

                    URL:<br> <input type="url" name="url" autocomplete="off" size="60"><br><br />

                    Description: <br><textarea name="description" maxlenght="256" cols="60" rows="5"></textarea>
                    <br><br />
                </form>
                <form action="upload.php" method="POST" enctype="multipart/form-data">
                    <div style="position:relative; width: 300px; height: 20px">
                        <p align="left">Attacthments (Select the file to upload):</p>
                    </div>
                    <br>
                    <input type="file" name="fileToUpload" id="fileToUpload">

                </form>
            </div>

            <div class="modal-footer">
                <div>
                    <input type="submit" class="btn btn-primary" name="submit" value="Submit" data-dismiss="modal">
                    <button type="button" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!---Modal for sign-up--->
<?php include "php/include/db.php";
      include "php/include/signup.php";       
    createAccount(); 
?>

<div id="signUp" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h1><b>Sign Up</b></h1>
                <p>Please fill in this form to create an account.</p>
            </div>
            <hr style="border: 0.6px solid #e2e2e2; margin: 6px 10px;">
                <div class="modal-body">
                <form action="php/include/signup.php" method="post" autocomplete="on">   
                    <div class="form-group">
                        <label for="firstname"><b>First Name</b></label><br>
                        <input type="text" class="form-control" placeholder="Enter First Name" name="firstname" required autofocus>
                    </div>

                    <div class="form-group">
                        <label for="lasttname"><b>Last Name</b></label><br>
                        <input type="text" class="form-control" placeholder="Enter Last Name" name="lastname" required>
                    </div>

                    <div class="form-group">
                        <label for="email"><b>Email</b></label><br>
                        <input type="text" class="form-control" placeholder="Enter Email" name="email" required>
                    </div>

                    <div class="form-group" >
                        <label for="password"><b>Password</b></label><br>
                        <input type="password" class="form-control" id="pwInput" placeholder="Enter Password"><input type="checkbox" onclick="showPW()">Show Password
                    </div>

                    <div class="form-group">
                        <label for="password"><b>Confirm Password</b></label><br>
                        <input type="password" class="form-control" name="password" placeholder="Confirm password" id="passconfirm" onkeyup="check();"><span id="message"></span>
                    </div>

                    <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

                <div class="modal-footer">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <input class="btn btn-primary" type="submit" name="submit" value="Submit" />
                </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!---Model plugin for login-->
<div class="modal fade" id="logIn" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h1><b>Login</b></h1>
                <p>Please enter your login credentials.</p>
            </div>
            <hr style="border: 0.6px solid #e2e2e2; margin: 3px 15px;">

            <div class="modal-body">
                <form action="php/include/login.php" method="post" autocomplete="on">
                <div class="form-group">
                    <label for="Email"><b>Email</b></label><br>
                    <input name="email" type="text" class="form-control" placeholder="Enter Email">
                </div>      
                
                <div class="form-group">
                    <label for="password"><b>Password</b></label><br>
                    <input name="password" type="password" class="form-control" placeholder="Enter Password">
                </div>                   
                
                <div class="checkbox">
                <label>
                    <input type="checkbox" checked="checked" name="remember"> Remember me
                </label>
                </div>

            <div class="modal-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <input type="submit" class="btn btn-primary" name="submit" value="Login">
            </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--modal for running python-->
<div class="modal fade" id="pythonModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Run Python Script</h4>
            </div>


                <form  enctype=“multipart/form-data”>
                    <div class="modal-body">
                        <label for="file"><b>Uploading file</b></label><br/>
                        <input type="file" id="myfile" name="myfile"><br/>               
                        
                        <img id="display_img" src="#" alt="your image" style='height: 100%; width: 100%; object-fit: contain'/>
                        
                        <div id="demo2"></div>
                        <div id="demo"></div>
                            <button type="button" class="btn btn-danger" onclick="loadDoc()">
                                <i></i>
                                Python button
                            </button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
        </div>
    </div>
</div>

<div class="modal fade" id="pythonUpload" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload File</h4>
            </div>


            <form enctype="multipart/form-data" action="lib/python/save_file.py" method="post">
                <p>File: <input type="file" name="filename" multiple="multiple"/></p>
                <p><input type="submit" value="Upload" /></p>
            </form>
        </div>
    </div>
</div>
