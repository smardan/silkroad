<?php 
/*
 * Author:  Somi Dasom Choi
 * Author email: dasom.choi@sait.ca
 * Created:  Feb.2019
 * 
 * (c) Copyright by Silk Road Team - CIRUS, ARIS, SAIT.
*/


//include "include/db.php";



$connect =pg_connect("host=sait.bgis.gismapping.ca port=5432 dbname=new_silk_road user=postgres password=password") or die("could not connect");

$output = '';

if (isset($_POST['searchVal']) && trim($_POST['searchVal'])!='' && strlen('searchVal') > 3){

	$searchq = $_POST['searchVal'];
	$searchq = preg_replace("#[^0-9a-z]#i","",$searchq);
//query	
    $query = pg_query("SELECT table_schema, table_name FROM information_schema.columns 
    WHERE table_schema iLIKE '%".$searchq."%'
    AND column_name= 'country1' ORDER BY table_name ASC") or die("could not search");
	
    $count = pg_num_rows($query);
	if($count == 0){
        $output = '<a style="width: 248px; background-color:white;">Data Not Found</a>';
        }else{
        $output = '<ul class="dropdown" style = "width: 268px;background-color: white;">';
            while($row = pg_fetch_array($query)){

                $output .= '<a class="searchresult" href="#"><li> '.$row['table_schema'].' - '.$row["table_name"].'</li></a>';
           }
        $output .= '</ul>';
       }
    }
echo($output); 
pg_close($connect);
?>