<?php 
/*
 * Author:  Somi Dasom Choi
 * Author email: dasom.choi@sait.ca
 * Created:  Feb.2019
 * 
 * (c) Copyright by Silk Road Team - CIRUS, ARIS, SAIT.
 */


include "php/include/db.php";
include "php/include/header.php";
include "php/include/navigation.php";?>  
	

<!--- map division --->	
<div id="mapdiv"></div>

<!--- side panel --->
	<div id="side_button" class="toggle-btn" >
		<span onclick="toggleSidepanel()">&#9776;</span>
	</div>

	<div class="col-md-2" id="side_panel">
		<div>
			<button class="form-control mybtn btn" onClick="dropDownFunc(this)" style="background-color:rgb(189, 189, 189);">
				<i class="glyphicon glyphicon-chevron-down"></i>
				<input type="checkbox" onClick="event.stopPropagation();toggleAll(this, 'toggleGr6')">
				<label id="nestedBtn">Regions</label>
			</button>
			<div class="dropDown w3-hide w3-border w3-round-xlarge" style="overflow-y: scroll;background-color:rgb(226, 221, 221);">
                <p class="Africa" style="font-weight:bold;margin:0;">Africa
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb1">
                        Algeria
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb2">
                        Burkina Faso
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb3">
                        Central African Republic
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb4">
                        Chad
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb5">
                        Egypt
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb6">
                        Eritrea
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb7">
                        Ghana
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb8">
                        Guinea
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr6" name="cb9">
                        Ivory Coast
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb10">
                        Libya
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb11">
                        Mali
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb12">
                        Mauritania
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb13">
                        Morocco
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb14">
                        Niger
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb15">
                        Nigeria
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb16">
                        Senegal
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb17">
                        Somalia
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb18">
                        Sudan
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb19">
                        Tanzania
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr1" name="cb20">
                        Tunisia
                    </form>

                <p class="Asia" style="font-weight:bold;margin:0;">Asia - Central
                    <form>
                        <input type="checkbox" class="toggleGr2" name="cb21">
                        Kazakhstan
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr2" name="cb23">
                        Kyrgyzstan
                    </form>

                    <form>
                        <input type="checkbox" class="toggleGr2" name="cb24">
                        Tajikistan
                    </form>

                    <form>
                        <input type="checkbox" class="toggleGr2" name="cb25">
                        Turkmenistan
                    </form>

                    <form>
                        <input type="checkbox" class="toggleGr2" name="cb26">
                        Uzbekistan
                    </form>
                    
                <p class="Asia" style="font-weight:bold;margin:0;">Asia - East
                    <form>
                        <input type="checkbox" class="toggleGr3" name="cb27">
                        China
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr3" name="cb28">
                        Japan
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr3" name="cb29">
                        Mongolia
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr3" name="cb30">
                        North Korea
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr3" name="cb31">
                        South Korea
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr3" name="cb32">
                        Taiwan
                    </form>
                    <form>
                        <input type="checkbox" class="toggleGr3" name="cb33">
                        Malaysia
                    </form>

                 <p class="Asia" style="font-weight:bold;margin:0;">Asia - South
                        <form>
                            <input type="checkbox" class="toggleGr4" name="cb34">
                            Afghanistan
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr4" name="cb35">
                            Bangladesh
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr4" name="cb36">
                            Bhutan
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr4" name="cb37">
                            India
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr4" name="cb38">
                            Nepal
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr4" name="cb39">
                            Pakistan
                        </form>
                        
                    <p class="Asia" style="font-weight:bold;margin:0;">Asia - Southeast
                        <form>
                            <input type="checkbox" class="toggleGr5" name="cb40">
                            Cambodia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr5" name="cb41">
                            Laos
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr5" name="cb42">
                            Myanmar/Burma
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr5" name="cb43">
                            Palestine
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr5" name="cb44">
                            Singapore
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr5" name="cb45">
                            Thailand
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr5" name="cb46">
                            Vietnam
                        </form>
                            
                    <p class="Middle East" style="font-weight:bold;margin:0;">Europe
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb47">
                            Albania
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb48">
                            Armenia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb49">
                            Austria
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb50">
                            Azerbaijan
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb51">
                            Belarus
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb52">
                            Belgium
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb53">
                            Bosnia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb54">
                            Bulgaria
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb55">
                            Croatia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb56">
                            Cyprus
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb57">
                            Czechia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb58">
                            Denmark
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb59">
                            Estonia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb60">
                            France
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb61">
                            Georgia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb62">
                            Germany
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb63">
                            Greece
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb64">
                            Hungary
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb65">
                            Italy
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb66">
                            Latvia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb67">
                            Lithuania
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb68">
                            Macedonia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb69">
                            Moldova
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb70">
                            Nertherlands
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb71">
                            Norway
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb72">
                            Poland
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb73">
                            Portugal
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb74">
                            Romania
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb75">
                            Russia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb76">
                            Serbia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb77">
                            Slovakia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb78">
                            Slovenia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb79">
                            Spain
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb80">
                            Sweden
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb81">
                            Switerland
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb82">
                            Turkey
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr6" name="cb83">
                            United Kingdom
                        </form>
                        
                 <p class="Middle East" style="font-weight:bold;margin:0;">Middle East
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb84">
                            Bahrain
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb85">
                            Iran
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb86">
                            Iraq
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb87">
                            Israel
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb88">
                            Jordan
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb89">
                            Kuwait
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb90">
                            Lebanon
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb91">
                            Oman
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb92">
                            Qatar
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb93">
                            Saudi Arabia
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb94">
                            Syria
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb95">
                            United Arab Emirates
                        </form>
                        <form>
                            <input type="checkbox" class="toggleGr7" name="cb96">
                            Yemen
                        </form>
            </div>
		</div>


		<div>
			<button class="form-control mybtn btn" onClick="dropDownFunc(this)" style="background-color:rgb(245, 158, 83);">
				<i class="glyphicon glyphicon-chevron-down"></i>
				<input type="checkbox" onClick="event.stopPropagation();toggleAll(this, 'toggleGr1')">
				<label id="nestedBtn">Road Usage</label>
			</button>
			<div class="dropDown w3-hide w3-border w3-round-xlarge">
				<form>
					<input type="checkbox" class="toggleGr8 toggleSR" name="cb1" checked>
					Trade Routes
				</form>

				<form>
					<input type="checkbox" class="toggleGr8" name="cb2">
					Major Roads
				</form>
				<form>
					<input type="checkbox" class="toggleGr8" name="cb1">
					Imperial Courier Routes
				</form>
				<form>
					<input type="checkbox" class="toggleGr8" name="cb1">
					Caravan Routes
				</form>
				<form>
					<input type="checkbox" class="toggleGr8" name="cb1">
					Pilgrimage Routes
				</form>
				<form>
					<input type="checkbox" class="toggleGr8" name="cb1">
					Transportation Routes
				</form>
			</div>
		</div>

		<div>
			<button class="form-control mybtn btn" onClick="dropDownFunc(this)" style="background-color:rgb(241, 161, 161);">

				<i class="glyphicon glyphicon-chevron-down"></i>
				<input type="checkbox" onClick="event.stopPropagation(); toggleAll(this, 'toggleGr2')">
				<label id="nestedBtn">Ancient Routes</label>

			</button>
			<div class="dropDown w3-hide w3-border w3-round-xlarge" style="background-color:rgb(237, 192, 192);">
			
                <p style="font-weight:bold;margin:0;">Route Type	
                
                <form>
					<input type="checkbox" class="toggleGr9 toggle_a_route_bt" name="cb2">
					Business Travel
				</form>     
                               
                <form>
					<input type="checkbox" class="toggleGr9 toggle_a_route_eb" name="cb2">
					Ecclesiastical Business
				</form>                                   
                				
				<form>
					<input type="checkbox" class="toggleGr9 toggle_a_route_messenger" name="cb2">
					Messenger
				</form>
				
				<form>
					<input type="checkbox" class="toggleGr9 toggle_a_route_military" name="cb2">
					Military Organisation
				</form>
				
				<form>
					<input type="checkbox" class="toggleGr9 toggle_a_route_pilgrimage" name="cb2">
					Pilgrimage
				</form>
				
				<form>
					<input type="checkbox" class="toggleGr9 toggle_a_route_trade" name="cb1">
					Trade 
				</form>
				
				<form>
					<input type="checkbox" class="toggleGr9 toggle_a_route_uk" name="cb2">
					Unknown
				</form>
			</div>
		</div>

		<div>
			<button class="form-control mybtn btn" onClick="dropDownFunc(this)" style="background-color:rgb(115, 184, 224);">

				<i class="glyphicon glyphicon-chevron-down"></i>
				<input type="checkbox" onClick="event.stopPropagation(); toggleAll(this, 'toggleGr4')">
				<label id="nestedBtn">World Heritage</label>

			</button>
			<div class="dropDown w3-hide w3-border w3-round-xlarge" style="overflow-y:scroll;background-color:rgb(174, 212, 234);">
                <form>
                    <input type="checkbox" class="toggleGr10 togglewheritage" name="cb1">
                    Beacon
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10" name="cb2">
                    Bedestan
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 togglewheritage" name="cb3">
                    Boat Bridge
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 togglewheritage" name="cb4">
                    Bridge
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 togglewheritage" name="cb5" checked>
                    Caravanserai
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 togglewheritage" name="cb6">
                    Citadel
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb7">
                    Ferry
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb8">
                    Fort
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb9">
                    Funduk/Fondaco
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb10">
                    Harbour
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb11">
                    Loggia
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb12">
                    Market/Bazaar
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb13">
                    Maybod Refrigerator
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb14">
                    Minaret
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb15">
                    Mosques
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb16">
                    Monastery
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb17">
                    Paper Mill
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb18">
                    Pigeon Tower
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb19">
                    Port
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb20">
                    Postal Station
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb21">
                    Public Bath
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb22">
                    Saradoba
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb23">
                    Shipyard
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb24">
                    Warehouse
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb25">
                    Watchtower
                </form>
                <form>
                    <input type="checkbox" class="toggleGr10 toggleCaravan" name="cb126">
                    Well
                </form>
            </div>
		</div>

		<div>
			<button class="form-control mybtn btn" onClick="dropDownFunc(this)" style="background-color:yellowgreen;">

				<i class="glyphicon glyphicon-chevron-down"></i>
				<input type="checkbox" onClick="event.stopPropagation(); toggleAll(this, 'toggleGr5')">
				<label id="nestedBtn">Ecology</label>
			</button>
			<div class="dropDown w3-hide w3-border w3-round-xlarge" style="background-color:#d0eb98;">
				<form>
					<input type="checkbox" class="toggleGr11" name="cb1">
					Land Cover
				</form>
				<form>
					<input type="checkbox" class="toggleGr11" name="cb2">
					Waterway
				</form>
				<form>
					<input type="checkbox" class="toggleGr11" name="cb3">
					Wildlife
				</form>
			</div>
		</div>
        
		<div>
			<button class="form-control mybtn btn" onClick="dropDownFunc(this)" style="background-color:rgb(211, 196, 79);">

				<i class="glyphicon glyphicon-chevron-down"></i>
				<input type="checkbox" onClick="event.stopPropagation(); toggleAll(this, 'toggleGr3')">
				<label id="nestedBtn">Mordern Routes</label>

			</button>
			<div class="dropDown w3-hide w3-border w3-round-xlarge" style="background-color:rgb(239, 229, 156);">
				<form>
					<input type="checkbox" class="toggleGr12" name="cb1">
					Cities
				</form>
				<form>
					<input type="checkbox" class="toggleGr12 toggleairp" name="cb2">
					Airports
				</form>
				<form>
					<input type="checkbox" class="toggleGr12 togglerail" name="cb3">
					Railways
				</form>		
				<form>
					<input type="checkbox" class="toggleGr12" name="cb4">
					Roads
				</form>
			</div>
		</div>

		<div>
			<button class="form-control mybtn btn" style="background-color:#6e69eb;">

				<i class="glyphicon glyphicon-download-alt"></i>
				<label id="nestedBtn">Download</label>
			</button>
		</div>

		<div>
			<button class="form-control mybtn btn" style="background-color:#ae2abf;">
				<i class="glyphicon glyphicon-share"></i>
				<label id="nestedBtn">Share</label>
			</button>
		</div>
                
	</div>

    <script type="text/javascript" id="cookieinfo"
	src="//cookieinfoscript.com/js/cookieinfo.min.js"
	data-height ="50px"
    data-bg="#645862"
	data-fg="#FFFFFF"
    data-position="top"
	data-link="#F1D600"
	data-cookie="CookieInfoScript"
	data-close-text="Got it!">
    </script> 
    
<?php include "php/include/footer.php"; ?>
