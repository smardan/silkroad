#! C:\Users\Admin\Anaconda3\envs\python-cvcourse\python.exe

import cgitb, cgi, os

from PIL import Image

import psycopg2

cgitb.enable()
form = cgi.FieldStorage()

print("Content-Type: text/html; charset=utf-8\n\n")


print("<html>")

print('<title>Python - CGI</title>')

print('<h1>hello world, my web python script works!!</h1>')

latitude = form.getvalue('latitude')
latitude = float(latitude)
longitude = form.getvalue('longitude')
longitude = float(longitude)

def myfunc(lat,long):
    return lat, long

print('<div style="clear: both"><h2 style="float: left">latitude is </h2> <h2 style="float: left" id="lat">{}</h2></div>'.format(latitude))
print('<div style="clear: both"><h2 style="float: left">longitude is </h2><h2 style="float: left" id="long">{}</h2></div>'.format(longitude))

print('</br>')
print('</br>')
print('<hr/>')

print('<h3>Check the map for the location of your coordinates</h3>')

if __name__ == '__main__':
    myfunc(latitude,longitude)


###############################################################################################    

dbname='landmine'
user='postgres'
password='password'
host='sait.bgis.gismapping.ca'
port='5432'
    
connect = "dbname=%s user=%s password=%s host=%s port=%s" % (dbname, user, password, host, port)

table = "public.feature_class"

###############################################################################################

def view():
    conn = psycopg2.connect(connect)
    cur=conn.cursor()
    cur.execute("SELECT * FROM %s" % table)
    rows=cur.fetchall()
    conn.close()
    return rows
    
###############################################################################################

rows = view()
cgi.test()




print('</br>')
print('</br>')

print("</html>")