#! C:\Users\Admin\Anaconda3\envs\duc_env\python.exe

import json, ogr
import cgitb, cgi, os


cgitb.enable()
form = cgi.FieldStorage()

driver = ogr.GetDriverByName('ESRI Shapefile')

shp_path = form['mypath']

data_source = driver.Open(shp_path, 0)

fc = {
    'type': 'FeatureCollection',
    'features': []
    }

lyr = data_source.GetLayer(0)
for feature in lyr:    
    fc['features'].append(feature.ExportToJson(as_object=True))
    
with open('testing_22.geojson', 'w') as f:
    json.dump(fc, f)

print("Content-Type: text/html; charset=utf-8\n\n")

print("<html>")

print('<title>Python - CGI</title>')

print('<h1>hello world, my web python script works!!</h1>')


###############################################################################################    


##if __name__ == '__main__':


print('</br>')
print('</br>')

print("</html>")