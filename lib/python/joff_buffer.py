import ogr, os

# SHAPEFILE PARAMETER
##shpfile = ("C:\\Users\\Joffrey\\Desktop\\OLDWORLDTRADEROUTES\\SHP\Merged Datasets\\AFR_Cities.shp")
shpfile = ("C:\\Users\\Admin\\Desktop\\Silk_Routes\\Cities.shp")
# NEW FILE NAME
saveas= ('friyay6666')
#Buffer Distances
userDist= (100)
print (userDist/10)

def createBuffer(inputfn, outputBufferfn, bufferDist):
    inputds = ogr.Open(inputfn)
    inputlyr = inputds.GetLayer()

    shpdriver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(outputBufferfn):
        shpdriver.DeleteDataSource(outputBufferfn)
    outputBufferds = shpdriver.CreateDataSource(outputBufferfn)
    bufferlyr = outputBufferds.CreateLayer(outputBufferfn, geom_type=ogr.wkbPolygon)
    featureDefn = bufferlyr.GetLayerDefn()

    for feature in inputlyr:
        ingeom = feature.GetGeometryRef()
        geomBuffer = ingeom.Buffer(bufferDist)

        outFeature = ogr.Feature(featureDefn)
        outFeature.SetGeometry(geomBuffer)
        bufferlyr.CreateFeature(outFeature)
        outFeature = None

def main(inputfn, outputBufferfn, bufferDist):
    createBuffer(inputfn, outputBufferfn, bufferDist)


if __name__ == "__main__":
    ##inputfn = 'C:\\Users\\Joffrey\\Desktop\\OLDWORLDTRADEROUTES\\SHP\\Merged Datasets\\AFR_Cities.shp'
    inputfn = "C:\\Users\\Admin\\Desktop\\Silk_Routes\\Cities.shp"
    ##outputBufferfn = 'C:\\Users\\Joffrey\\Desktop\\OLDWORLDTRADEROUTES\\SHP\\'+saveas+'.shp'
    outputBufferfn = 'C:\\Users\\Admin\\Desktop\\Silk_Routes\\'+saveas+'.shp'
    bufferDist = 0.0019

    main(inputfn, outputBufferfn, bufferDist)

print ('check ',outputBufferfn)