
//*****
//* Author:  Somi Dasom Choi
//* Author email: dasom.choi@sait.ca
//* Created:  Feb.2019
//* 
//* (c) Copyright by Silk Road Team - CIRUS, ARIS, SAIT.
//*****//


//**==walkthrough==**//

(function(){
    var tour = new Tour({
    storage: false
});

var tour = new Tour({
    steps:[
    {   element: "#mapdiv",    
        placement: "top",
        backdrop: true,  
        title: "Welcome to CIRUS-Silk Road web application",
        content: "This tour will guide you through some of features we'd like to point out"
    },
        
    {   element: ".search",
        placement: "bottom",
        backdrop: true,
        backdropPadding: 8,    
        content: "Want to know what we have in the database? Search what kinds of data available through the database."
//        template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='prev'>« Prev</button><span data-role='separator'>|</span><button class='btn btn-default' data-role='next'>Next »</button></div><button class='btn btn-default' data-role='end'>Skip</button></div>"
  },
        
    {   element: ".pips-slider",
        title: "Time Slider",
        placement: "top",
        backdrop: true,
        backdropPadding: 15,
        content: "Drag the slider and select the time range. You'll see data changed."        
    }
    ]
});

tour.init();
tour.start();

}());


//**==search function==**//
function searchq() {
    var searchTxt = $("input[name='search']").val();

    $.post("php/fetch.php", {
        searchVal: searchTxt
    }, function (output) {
        $("#output").html(output);

    });
}

//**==sign up & login function==**//
//**==show password in sign up form==**//
function showPW() {
    var x = document.getElementById("pwInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
    var y =document.getElementById("passconfirm");
    if (y.type === "password") {
        y.type = "text";
    } else {
        y.type = "password";
    }
}

//**==password match message==**//
var password = document.getElementById("pwInput");
var confirmpassword = document.getElementById("passconfirm");
var passwordmatchmessage = document.getElementById("message");

var check = function () {
    if (password.value == confirmpassword.value) {
        passwordmatchmessage.style.color = 'green';
        passwordmatchmessage.innerHTML = "Password matches!";
    } else {
        passwordmatchmessage.style.color = 'red';
        passwordmatchmessage.innerHTML = "Password doesn't match!"
    }
}


//**==Initializing basemaps==**//
var positronURL = 'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png';
var positronAttrib = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>';

var topoLink = '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
var topoURL = 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png'
var topoAttrib = 'Map data: &copy; ' + topoLink;

var hybridURL = 'http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}';

var tonerAttrib = 'Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under ODbL. <COPY HTML>'

var darkURL = 'https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png';
var darkAttrib = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>';


var positronMap = L.tileLayer(positronURL, {
    attribution: positronAttrib,
    subdomains: 'abcd'
});

var topoMap = L.tileLayer(topoURL, {
    attribution: topoAttrib
});

var hybridMap = L.tileLayer(hybridURL, {
    maxZoom: 22.5,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
});

var tonerMap = new L.StamenTileLayer("toner", {
    attribution: tonerAttrib
});

var darkMap = L.tileLayer(darkURL, {
    attribution: darkAttrib,
    subdomains: 'abcd'
});


var mymap;

var mymap = L.map('mapdiv', {
    attributionContorol: false,
    zoomControl: false,
    center: [21, 64],
    zoom: 3.9,
    minZoom: 2.6,

    //default basemap-topographic map
    layers: [positronMap]
});

var baseMaps = {
    "Open Street Map": positronMap,
    "Topographic Map": topoMap,
    "Satellite Map": hybridMap,
    "Stamen Toner Map": tonerMap,
    "Dark Map": darkMap
};

//L.control.layers(baseMaps).addTo(mymap);

//**==Adding historic georeferenced maps (in a png form)==**//
// var MediPath = "img/hisotric_maps/Mediterranean_georef.png";
// var MediBounds = [[46.7549166193, -13.9306640625], [29.8597014421, 36.8865966797]] 

// var a = L.imageOverlay(MediPath, MediBounds, {opacity:0.35}).addTo(mymap);

//ESPG:4326
// var trwPath = "img/historic_maps/TR_West_Georef.png";
// var trwBounds = [[43.002, 24.532], [33.616, 32.7812]] 
// var trw = L.imageOverlay(trwPath, trwBounds, {opacity:0.45, interactive:true}).addTo(mymap);

// var trmPath = "img/historic_maps/TR_Mid_Georef.png";
// var trmBounds = [[42.892, 31.589], [33.642, 39.619]] 
// var trm = L.imageOverlay(trmPath, trmBounds, {opacity:0.45}).addTo(mymap);

// var trePath = "img/historic_maps/TR_East_Georef.png";
// var treBounds = [[42.873, 38.571], [33.30, 46.674]] 
// var tre = L.imageOverlay(trePath, treBounds, {opacity:0.4}).addTo(mymap);

//**===mouseover with lat, long ===**//
mymap.on('mousemove', function (e) {
    var str = "Latitude: " + e.latlng.lat.toFixed(3) + ",      " + "Longitude: " + e.latlng.lng.toFixed(3) + "   " + " Zoom Level: " + mymap.getZoom();
    $("#map_coords").html(str);

});


//**==time slider with pips & labels **==//

var years = [
     "600BCE", "500BCE", "400BCE", "300BCE", "200BCE", "100BCE", "0",
     "100CE", "200CE", "300CE", "400CE", "500CE", "600CE", "700CE", "800CE", "900CE", "1000CE",
     "1100CE", "1200CE", "1300CE", "1400CE", "1500CE", "1600 CE", "1700CE", "1800CE", "1900CE", "2000CE", "2100CE",
 ];

 $(function() {
     $("#pips-slider")
         .slider({
             range: true,
             min: 0,
             max: 27,
             values: [ 4, 20 ],
             step: 1
         })
         .slider("pips", {
             rest: "label",
             step: 6,
             labels: years        
         })
         .slider("float", {       	
             labels: years
         });
 });       

//**=== Create additional Control placeholders===**//
function addControlPlaceholders(mymap) {
    var corners = mymap._controlCorners,
        l = 'leaflet-',
        container = mymap._controlContainer;

    function createCorner(vSide, hSide) {
        var className = l + vSide + ' ' + l + hSide;

        corners[vSide + hSide] = L.DomUtil.create('div', className, container);
    }

    createCorner('verticalbottom', 'left');
    createCorner('verticalbottom', 'right');
}
addControlPlaceholders(mymap);

function addControlPlaceholders2(mymap) {
    var corners2 = mymap._controlCorners,
        l2 = 'leaflet-',
        container2 = mymap._controlContainer;

    function createCorner2(vSide, hSide) {
        var className = l2 + vSide + ' ' + l2 + hSide;

        corners2[vSide + hSide] = L.DomUtil.create('div', className, container2);
    }

    createCorner2('attributionbottom', 'left');
    createCorner2('attributionbottom', 'right');
}
addControlPlaceholders2(mymap);


//**===geocoding search bar===**//
var searchControl = L.esri.Geocoding.geosearch({
    position: "topright"
}).addTo(mymap);

 // Adding layergroups to search control

var results = L.layerGroup().addTo(mymap);

searchControl.on('results', function (data) {
    results.clearLayers();
    for (var i = data.results.length - 1; i >= 0; i--) {

        results.addLayer(L.marker(data.results[i].latlng));
    }
});


//**===measure tool===**//
var ctlMeasure = L.control.measure({
    position: 'topright',
    //whether to ise keyboard control
    keyboard: true,
    //shortcut to activate measure
    activateKeyCode: 'M'.charCodeAt(0),
    //shortcut to cancel measure, defaults to 'Esc'
    cancelKeyCode: 27,
    lineColor: 'red',
    lineWeight: 2,
    lineDashArray: '6,6',
    lineOpacity: 1
}).addTo(mymap);


//**===zoomhome control===**//
var zoomHome = L.Control.zoomHome({
    position: 'verticalbottomright'
});
zoomHome.addTo(mymap);

L.control.attribution({
    position: 'attributionbottomright'
}).addTo(mymap);


//**===scale bar===**//
var ctlScale = L.control.scale({
    metric: true,
    maxWidth: 200,
    position: 'verticalbottomleft'
}).addTo(mymap);



//*** hover & zoom-in effect on the flags in the footer***///
 //*hover
 $('.imageCS').mouseover(function () {
     $(this).css('opacity', '.2');
 }).mouseout(function () {
     $(this).css('opacity', '1');
 });
 
 //*zoom
 function newLocation(newLat, newLng, newZoom) {
     var newCoord = new L.LatLng(newLat, newLng)
     mymap.setView(newCoord, newZoom);
 }

 //Setting Location with jQuery
 $(document).ready(function () {
     $("#afg").on('click', function () {
         newLocation(33.9391, 67.71, 6)
     });
     
     $("#alb").on('click', function () {
         newLocation(41, 20.1683, 7.3)
     });
     
     $("#are").on('click', function () {
         newLocation(23.4241, 53.8478, 7.3)
     });
     
     $("#arm").on('click', function () {
         newLocation(40.0691, 45.0382, 7.5)
     });
     
     $("#aut").on('click', function () {
         newLocation(47.5162, 14.5501, 7)
     });
     
     $("#aze").on('click', function () {
         newLocation(40.1431, 47.5769, 7.5)
     });
     
     $("#bel").on('click', function () {
         newLocation(50.5039, 4.4699, 7.3)
     });
     
     $("#bfa").on('click', function () {
         newLocation(12, -1.5, 7.2)
     });
     
     $("#bgd").on('click', function () {
         newLocation(23.6850, 90.3563, 7.3)
     });
     
     $("#bgr").on('click', function () {
         newLocation(42.7339, 25.4858, 7)
     });
     
     $("#bhr").on('click', function () {
         newLocation(26.0667, 50.5577, 10.2)
     });
     
     $("#bih").on('click', function () {
         newLocation(43.9159, 17.6791, 7.2)
     });
     
     $("#blr").on('click', function () {
         newLocation(53.5098, 27.9534, 7.2)
     });
     
     $("#btn").on('click', function () {
         newLocation(27.5142, 90.4336, 8.3)
     });
     
     $("#caf").on('click', function () {
         newLocation(6.4111, 20.9394, 7.2)
     });
     
     $("#che").on('click', function () {
         newLocation(46.6182, 8.2275, 7.2)
     });
     
     $("#chn").on('click', function () {
         newLocation(30, 104.1954, 5)
     });
     
     $("#civ").on('click', function () {
         newLocation(7.4400, -5.5471, 7.2)
     });
     
     $("#cyp").on('click', function () {
         newLocation(35.1264, 33.4299, 8.2)
     });
     
     $("#cze").on('click', function () {
         newLocation(49.8175, 15.4730, 7)
     });
     
     $("#deu").on('click', function () {
         newLocation(51.1657, 10.4515, 7)
     });
     
     $("#dnk").on('click', function () {
         newLocation(55.6639, 9.5018, 7.2)
     });
     
     $("#dza").on('click', function () {
         newLocation(26.7, -1.6596, 5.5)
     });
     
     $("#egy").on('click', function () {
         newLocation(26.8206, 30.8025, 6.7)
     });
     
     $("#eri").on('click', function () {
         newLocation(15.1794, 39.7823, 7.1)
     });
     
     $("#esp").on('click', function () {
         newLocation(40, -3.7492, 6.3)
     });
     
     $("#est").on('click', function () {
         newLocation(58.5953, 25.0136, 7.2)
     });
     
     $("#eth").on('click', function () {
         newLocation(8.7, 40.4897, 6)
     });
     
     $("#fra").on('click', function () {
         newLocation(46, 2.2137, 6.3)
     });
     
     $("#gbr").on('click', function () {
         newLocation(54, -3.4360, 6)
     });
     
     $("#geo").on('click', function () {
         newLocation(42, 43.3569, 7.2)
     });
     
     $("#gha").on('click', function () {
         newLocation(7.2, -1.0232, 7.2)
     });
     
     $("#gin").on('click', function () {
         newLocation(9.4, -11.6, 7.4)
     });
     
     $("#grc").on('click', function () {
         newLocation(39.0742, 21.8243, 7)
     });
     
     $("#hrv").on('click', function () {
         newLocation(44.7, 15.2000, 6.5)
     });

     $("#hun").on('click', function () {
         newLocation(47, 19.5033, 7)
     });
     
     $("#ind").on('click', function () {
         newLocation(20.5937, 78.9629, 5.5)
     });
     
     $("#irn").on('click', function () {
         newLocation(30, 53.6880, 6)
     });
     
     $("#irq").on('click', function () {
         newLocation(33.2232, 43.6793, 7)
     });
     
     $("#isr").on('click', function () {
         newLocation(31.0461, 34.8516, 8)
     });
     
     $("#ita").on('click', function () {
         newLocation(41.8719, 12.5674, 6)
     });
     
     $("#jor").on('click', function () {
         newLocation(30.5852, 36.2384, 7.2)
     });
     
     $("#jpn").on('click', function () {
         newLocation(36.2048, 138.2529, 6)
     });
     
     $("#kaz").on('click', function () {
         newLocation(48.0196, 66.9237, 6)
     });
     
     $("#kgz").on('click', function () {
         newLocation(41.2044, 74.7661, 7)
     });
     
     $("#khm").on('click', function () {
         newLocation(12.5657, 104.9910, 7.2)
     });
     
     $("#kor").on('click', function () {
         newLocation(35.9078, 127.7669, 6)
     });
     
     $("#kwt").on('click', function () {
         newLocation(29.3117, 47.4818, 8)
     });
     
     $("#lao").on('click', function () {
         newLocation(19.8563, 102.4955, 7)
     });
     
     $("#lbn").on('click', function () {
         newLocation(33.8547, 35.8623, 8)
     });
     
     $("#lby").on('click', function () {
         newLocation(26, 17.2283, 6)
     });
     
     $("#ltu").on('click', function () {
         newLocation(55.1694, 23.8813, 7.1)
     });
     
     $("#lva").on('click', function () {
         newLocation(56.8796, 24.6032, 7)
     });
     
     $("#mar").on('click', function () {
         newLocation(31.7917, -7.0926, 7.1)
     });
     
     $("#mda").on('click', function () {
         newLocation(47, 28.3699, 7.4)
     });
     
     $("#mkd").on('click', function () {
         newLocation(41.6086, 21.7453, 7.3)
     });
     
     $("#mli").on('click', function () {
         newLocation(16.8, -3.9962, 6)
     });
     
     $("#mmr").on('click', function () {
         newLocation(20, 95.9560, 6.5)
     });
     
     $("#mng").on('click', function () {
         newLocation(45.5, 103.8467, 6)
     });
     
     $("#mrt").on('click', function () {
         newLocation(20.2, -10.9408, 6)
     });
     
     $("#mys").on('click', function () {
         newLocation(4.2105, 101.9758, 6)
     });
        
     $("#ner").on('click', function () {
         newLocation(17.6078, 8.0817, 6.4)
     });
     
     $("#nga").on('click', function () {
         newLocation(8.7, 8.6753, 6.4)
     });
     
     $("#nld").on('click', function () {
         newLocation(52.1326, 5.2913, 7)
     });
     
     $("#nor").on('click', function () {
         newLocation(63, 8.4689, 5)
     });
     
     $("#npl").on('click', function () {
         newLocation(28.3949, 84.1240, 7)
     });
     
     $("#omn").on('click', function () {
         newLocation(20, 55.9754, 7)
     });
     
     $("#pak").on('click', function () {
         newLocation(30.3753, 69.3451, 7)
     });
     
     $("#pol").on('click', function () {
         newLocation(51.9194, 19.1451, 6)
     });
     
     $("#prk").on('click', function () {
         newLocation(40.3399, 127.5101, 6)
     });
 
     $("#prt").on('click', function () {
         newLocation(39.3999, -8.2245, 7)
     });
     
     $("#pse").on('click', function () {
         newLocation(31.9522, 35.2332, 7)
     });
     
     $("#qat").on('click', function () {
         newLocation(25, 51.1839, 8.5)
     });
     
     $("#rou").on('click', function () {
         newLocation(45.6, 24.9668, 6.5)
     });
     
     $("#rus").on('click', function () {
         newLocation(61.5240, 102, 3.5)
     });
     
     $("#sau").on('click', function () {
         newLocation(23.8859, 45.0792, 5.5)
     });
     
     $("#sdn").on('click', function () {
         newLocation(13, 30.2176, 5.5)
     });
     
     $("#sen").on('click', function () {
         newLocation(14.4974, -14.4524, 6.8)
     });
     
     $("#sgp").on('click', function () {
         newLocation(1.3521, 103.8198, 11)
     });
     
     $("#som").on('click', function () {
         newLocation(3.5, 46.1996, 6.7)
     });
     
     $("#srb").on('click', function () {
         newLocation(44.0165, 21.0059, 7)
     });
     
     $("#svk").on('click', function () {
         newLocation(48.6690, 19.6990, 6.5)
     });
     
     $("#svn").on('click', function () {
         newLocation(46.1512, 14.9955, 7.2)
     });
     
     $("#swe").on('click', function () {
         newLocation(60.1282, 18.6435, 5)
     });
     
     $("#syr").on('click', function () {
         newLocation(34.8021, 38.9968, 6.5)
     });
     
     $("#tcd").on('click', function () {
         newLocation(15.4542, 18.7322, 6)
     });
     
     $("#tha").on('click', function () {
         newLocation(15.4542, 100.9925, 7)
     });
     
     $("#tjk").on('click', function () {
         newLocation(38, 71.2761, 7)
     });
     
     $("#tkm").on('click', function () {
         newLocation(38.9697, 59.5563, 7)
     });
     
     $("#tun").on('click', function () {
         newLocation(33, 9.5375, 7.1)
     });
     
     $("#tur").on('click', function () {
         newLocation(38.9637, 35.2433, 6)
     });
     
     $("#twn").on('click', function () {
         newLocation(23.6978, 120.9605, 7)
     });
     
     $("#tza").on('click', function () {
         newLocation(-6, 34.8888, 7)
     });
     
     $("#uzb").on('click', function () {
         newLocation(41.3775, 64.5853, 6.2)
     });
     
     $("#vnm").on('click', function () {
         newLocation(14.0583, 108.2772, 6.2)
     });
     
     $("#yem").on('click', function () {
         newLocation(15.5527, 48.5164, 6.5)
     });
 });

//****script needed to reference php to load data from PostGIS database****// 

//**====World Heritage. historic structures====**/
var wheritageStyle = {
    radius: 4,
    fillColor: "#0d73be",
    color: "rgba(56, 129, 172, 0.8)",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.85
};

var markers_wheritage = L.markerClusterGroup({
    removeOutsideVisibleBounds: false,
    polygonOptions: {
        fillColor: 'rgba(56, 129, 172, 0.5)',
        color: 'rgba(56, 129, 172, 0.8)',
        weight: 3,
        opacity: 1,
        fillOpacity: 0.5
    },

    iconCreateFunction: function (cluster) {
        return L.divIcon({
            html: '<div style="background-clip: padding-box; background-color:rgba(56, 129, 172, 0.8); width: 40px; height: 40px; margin-left: -15px; margin-top: -18px; padding-top: 6px; text-align: center; border-radius: 20px; border: 5px solid rgba(115, 184, 224, 0.6); font-size:12px "Helvetica Neue", Arial, Helvetica, sans-serif;">' + cluster.getChildCount() + '</div>',
            iconSize: [0, 0]
        });
    }
});

markers_wheritage.on('', function (e) {
    console.log(e);
    document.getElementById("log").innerHTML = "";
});

$.ajax({
    url: 'php/getData_historic_structures.php',
    success: function (response) {

        if (wheritage) {
            mymap.removeLayer(wheritage)
        };

        var wheritage = L.geoJSON(JSON.parse(response), {
            pointToLayer: function (feature, latlng) {
                //popup configuration
                var strPopup = "<h4>htthttpth" + feature.properties.name + "</h4><hr>";
                strPopup += "<h5>*Country: " + feature.properties.country1 + "</h5>";
                strPopup += "<h5>*Detailed location: " + feature.properties.location + "</h5>";
                strPopup += "<h5>*Date built: " + feature.properties.date_built + "</h5>";
                strPopup += "<p>Click to view larger photo</p>"
                strPopup += "<img src='" + feature.properties.url + "' width='300px'>";

                strPopup += "<h5>*Type: " + feature.properties.type + "</h5>";
                
                

                //target="_blank"->open in new brower tab
                strPopup += "<a href='" + feature.properties.link + "' target='blank'>More Info";

                strPopup += "</a>";

                return L.circleMarker(latlng, wheritageStyle).bindPopup(strPopup);
                wheritage.on('click', function (e) {
                    document.getElementById("log").innerHTML = "World Heritage Structure" + e.target.strPopup;
                });

            }
        });
        markers_wheritage.addLayer(wheritage);
        mymap.addLayer(markers_wheritage);
        //                window.markers_caravan = markers_caravan  
    }
});
markers_wheritage.addTo(mymap);


var wheritageStatus = 1;
$(".togglewheritage").on("click", function () {

    if (wheritageStatus == 0) {
        markers_wheritage.addTo(mymap);
        wheritageStatus = 1;

    } else if (wheritageStatus == 1) {
        mymap.removeLayer(markers_wheritage);
        wheritageStatus = 0;
    }

});


//**======================Airport points=======================**// 
var airpStyle = {
    radius: 5,
    fillColor: "rgb(255, 226, 0)",
    color: "rgb(170, 151, 7)",
    weight: 1,
    opacity: 1
};

var markers_airp = L.markerClusterGroup({
    removeOutsideVisibleBounds: false,
    polygonOptions: {
        fillColor: '#e2ce98',
        color: 'rgba(240, 194, 12, 0.5)',
        weight: 3,
        opacity: 1,
        fillOpacity: 0.5
    },

    iconCreateFunction: function (cluster) {
        return L.divIcon({
            html: '<div style="background-clip: padding-box; background-color:rgba(240, 194, 12, 0.85); width: 40px; height: 40px; margin-left: -15px; margin-top: -18px; padding-top: 6px; text-align: center; border-radius: 20px; border: 5px solid rgba(241, 211, 87, 0.6); font-size:12px "Helvetica Neue", Arial, Helvetica, sans-serif;">' + cluster.getChildCount() + '</div>',
            iconSize: [0, 0]
        });
    }
});

markers_airp.on('', function (e) {
    console.log(e);
    document.getElementById("log").innerHTML = "";
});


$.ajax({
    url: 'php/getData_airp.php',
    success: function (response) {
//               if (airp) {
//                   mymap.removeLayer(airp)
//               };
//               
        var airp = L.geoJSON(JSON.parse(response), {
            pointToLayer: function (feature, latlng) {   
                return L.circleMarker(latlng, airpStyle);
            }
        });
        
        markers_airp.addLayer(airp);
        mymap.addLayer(markers_airp);
        
        mymap.removeLayer(markers_airp);
       // window.airp = airp
    }
});

var airpStatus = 0;
$(".toggleairp").on("click", function () {
    if (airpStatus == 0) {
       markers_airp.addTo(mymap);
        airpStatus = 1;
    } else if (airpStatus == 1) {
        mymap.removeLayer(markers_airp);
        airpStatus = 0;
    }
});


//**=========Modern railways==========//      
var railStyle = {
    color: "black",
    opacity: 0.7,
    weight: 0.6
};

var rail;
$.ajax({
    url: 'php/getData_rails.php',
    type: 'POST',
    success: function (response) {
        //                if (rd) {
        //                    mymap.removeLayer(rd)
        //                };

        var rail = L.geoJSON(JSON.parse(response), {
            style: railStyle
        });
        ///zoom dependent visible range
        //                function rd_WithZoom() {
        //                    if (mymap.getZoom() >= 7) {
        //                        rd.addTo(mymap);
        //                    } else {
        //                        mymap.removeLayer(afgrd);
        //                    }
        //                };
        //
        //                {
        //                    mymap.on('zoom', function() {
        //                        rd_WithZoom();
        //                    })
        //                }
        //
        window.rail = rail
    }

});

var railStatus = 0;
$(".togglerail").on("click", function () {
    if (railStatus == 0) {
        rail.addTo(mymap);
        railStatus = 1;
    } else if (railStatus == 1) {
        mymap.removeLayer(rail);
        railStatus = 0;
    }

});


//**===================COUNTRY BOUNDARY==========================**//
var bnStyle = {
    fillcolor: "#dda936",
    fillopacity: 0.5,
    color: "rgba(204, 169, 90, 0.97)",
    weight: 0.6
};
//		          var countryBn;
//		            $.ajax({
//		              url: 'php/getData_countryBn.php',
//		              type: 'POST',
//		              success: function(response) {
//		                  var countryBn = L.geoJSON(JSON.parse(response), {style: bnStyle}).addTo(mymap);
//		          }});  
//		    


//**=========Silk Road Multiline String ================**// 
var silkroadStyle = {
    color: "#c7711a",
    opacity: 1,
    weight: 1

};
var silkroad;
$.ajax({
    url: 'php/getData_silk_road.php',
    type: 'POST',
    success: function (response) {
        var silkroad = L.geoJSON(JSON.parse(response), {
            style: silkroadStyle
        }).addTo(mymap);

        window.silkroad = silkroad
    }
});

var srStatus = 1;
$(".toggleSR").on("click", function () {
    if (srStatus == 0) {
        silkroad.addTo(mymap);
        srStatus = 1;
    } else if (srStatus == 1) {
        mymap.removeLayer(silkroad);
        srStatus = 0;
    }

});

//**========Ancient Cities=**//

//    var a_citiesStyle = {
//           radius: 5,
//           fillcolor: "#d32296",
//           fillopacity: 1,
//           color: "#d32296",
//           weight: 0.6
//
//       };
//       var a_cities;
//       $.ajax({
//           url: 'php/getData_ancient_cities.php',
//           type: 'POST',
//           success: function (response) {
//               var a_cities = L.geoJSON(JSON.parse(response), {
//                   style: a_citiesStyle
//               });
//
//               window.a_cities = a_cities
//           }
//       });
//
//       var a_citiesStatus = 0;
//       $(".toggle_a_cities").on("click", function () {
//           if (a_citiesStatus == 0) {
//               a_cities.addTo(mymap);
//               a_citiesStatus = 1;
//               
//           } else if (a_citiesStatus == 1) {
//               mymap.removeLayer(a_cities);
//               a_citiesStatus = 0;
//          }
//
//       });
//

//**===Ancient Routes -by route type===***//

//**======Type - business travel====**//
var a_route_btStyle = {
    color: "#d32296",
    opacity: 1,
    weight: 0.5

};
var a_route_bt;
$.ajax({
    url: 'php/getData_a_route_bt.php',
    type: 'POST',
    success: function (response) {
        var a_route_bt = L.geoJSON(JSON.parse(response), {
            style: a_route_btStyle
        });

        window.a_route_bt = a_route_bt
    }
});

var a_route_btStatus = 0;
$(".toggle_a_route_bt").on("click", function () {
    if (a_route_btStatus == 0) {
        a_route_bt.addTo(mymap);
        a_route_btStatus = 1;
        
    } else if (a_route_btStatus == 1) {
        mymap.removeLayer(a_route_bt);
        a_route_btStatus = 0;
    }
});


//**======Type - Ecclesiastical Business====**//
var a_route_ebStyle = {
    color: "#d32296",
    opacity: 1,
    weight: 0.5

};
var a_route_eb;
$.ajax({
    url: 'php/getData_a_route_eb.php',
    type: 'POST',
    success: function (response) {
        var a_route_eb = L.geoJSON(JSON.parse(response), {
            style: a_route_ebStyle
        });

        window.a_route_eb = a_route_eb
    }
});

var a_route_ebStatus = 0;
$(".toggle_a_route_eb").on("click", function () {
    if (a_route_ebStatus == 0) {
        a_route_eb.addTo(mymap);
        a_route_ebStatus = 1;
        
    } else if (a_route_ebStatus == 1) {
        mymap.removeLayer(a_route_eb);
        a_route_ebStatus = 0;
    }

});

//**======Type - messenger====**//
var a_route_messengerStyle = {
    color: "#d32296",
    opacity: 1,
    weight: 0.3

};
var a_route_messenger;
$.ajax({
    url: 'php/getData_a_route_mess.php',
    type: 'POST',
    success: function (response) {
        var a_route_messenger = L.geoJSON(JSON.parse(response), {
            style: a_route_messengerStyle
        });

        window.a_route_messenger = a_route_messenger
    }
});

var a_route_messengerStatus = 0;
$(".toggle_a_route_messenger").on("click", function () {
    if (a_route_messengerStatus == 0) {
        a_route_messenger.addTo(mymap);
        a_route_messengerStatus = 1;
        
    } else if (a_route_messengerStatus == 1) {
        mymap.removeLayer(a_route_messenger);
        a_route_messengerStatus = 0;
    }

});

 //**======Type - Military Organisation====**//
var a_route_militaryStyle = {
    color: "#d32296",
    opacity: 1,
    weight: 0.5

};
var a_route_military;
$.ajax({
    url: 'php/getData_a_route_military.php',
    type: 'POST',
    success: function (response) {
        var a_route_military = L.geoJSON(JSON.parse(response), {
            style: a_route_militaryStyle
        });

        window.a_route_military = a_route_military
    }
});

var a_route_militaryStatus = 0;
$(".toggle_a_route_military").on("click", function () {
    if (a_route_militaryStatus == 0) {
        a_route_military.addTo(mymap);
        a_route_militaryStatus = 1;
        
    } else if (a_route_militaryStatus == 1) {
        mymap.removeLayer(a_route_military);
        a_route_militaryStatus = 0;
    }

});

//**===Type - pilgrimage
var a_route_pilgrimageStyle = {
    color: "#0d6d79",
    opacity: 1,
    weight: 0.5

};
var a_route_pilgrimage;
$.ajax({
    url: 'php/getData_a_route_pilgrimage.php',
    type: 'POST',
    success: function (response) {
        var a_route_pilgrimage = L.geoJSON(JSON.parse(response), {
            style: a_route_pilgrimageStyle
        });

        window.a_route_pilgrimage = a_route_pilgrimage
    }
});

var a_route_pilgrimageStatus = 0;
$(".toggle_a_route_pilgrimage").on("click", function () {
    if (a_route_pilgrimageStatus == 0) {
        a_route_pilgrimage.addTo(mymap);
        a_route_pilgrimageStatus = 1;
        
    } else if (a_route_pilgrimageStatus == 1) {
        mymap.removeLayer(a_route_pilgrimage);
        a_route_pilgrimageStatus = 0;
    }

});


//**====Type - trade
var a_route_tradeStyle = {
    color: "#3022d3",
    opacity: 1,
    weight: 0.3

};
var a_route_trade;
$.ajax({
    url: 'php/getData_a_route_trade.php',
    type: 'POST',
    success: function (response) {
        var a_route_trade = L.geoJSON(JSON.parse(response), {
            style: a_route_tradeStyle
        });

        window.a_route_trade = a_route_trade
    }
});

var a_route_tradeStatus = 0;
$(".toggle_a_route_trade").on("click", function () {
    if (a_route_tradeStatus == 0) {
        a_route_trade.addTo(mymap);
        a_route_tradeStatus = 1;
        
    } else if (a_route_tradeStatus == 1) {
        mymap.removeLayer(a_route_trade);
        a_route_tradeStatus = 0;
    }

});

//**=====Type - unknown
var a_route_ukStyle = {
    color: "#0d6d79",
    opacity: 1,
    weight: 0.5

};
var a_route_uk;
$.ajax({
    url: 'php/getData_a_route_uk.php',
    type: 'POST',
    success: function (response) {
        var a_route_uk = L.geoJSON(JSON.parse(response), {
            style: a_route_ukStyle
        });

        window.a_route_uk = a_route_uk
    }
});

var a_route_ukStatus = 0;
$(".toggle_a_route_uk").on("click", function () {
    if (a_route_ukStatus == 0) {
        a_route_uk.addTo(mymap);
        a_route_ukStatus = 1;
        
    } else if (a_route_ukStatus == 1) {
        mymap.removeLayer(a_route_uk);
        a_route_ukStatus = 0;
    }

});

//**====Side panel========**//
function toggleSidepanel() {
    document.getElementById("side_panel").classList.toggle("active");
    document.getElementById("side_button").classList.toggle("active");
}

toggleSidepanel();

function dropDownFunc(myDiv) {
    var x = $(myDiv).parent().children(".dropDown")[0];
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += "";
    } else {
        x.className = x.className.replace(" w3-show", "");
        x.previousElementSibling.className =
            x.previousElementSibling.className.replace("", "");
    }
}

function toggleAll(master, cn) {

    var cbarray = document.getElementsByClassName(cn);

    var master = $(master)[0].checked;;
    for (var i = 0, n = cbarray.length; i < n; i++) {
        if (master != cbarray[i].checked)
            $(cbarray[i]).trigger("click");
    }
}
